package com.supakit.bmi

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import com.supakit.bmi.databinding.ActivityMainBinding
import java.text.NumberFormat

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.buttonCalculate.setOnClickListener{ calculateBMI()}
        binding.weightOfServiceEditText.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view, keyCode)}
        binding.heightOfServiceEditText.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view, keyCode)}
    }
    private fun display(bmi: Double){
        val result = String.format("%.2f",bmi)
        binding.displayBmi.text = "Your BMI : "+result
    }
    private fun calculateBMI(){
        val stringIndexFieldW = binding.weightOfServiceEditText.text.toString()
        val weight = stringIndexFieldW.toDouble()
        val stringIndexFieldH = binding.heightOfServiceEditText.text.toString()
        val height = stringIndexFieldH.toDouble()
        var result = weight/(height*0.01*height*0.01)
        val selectedImage : ImageView = findViewById(R.id.imageView)
        if(result < 18.5){
            selectedImage.setImageResource(R.drawable.select_1)
            binding.displayDetail.text = "อยู่ในเกณท์ : น้ำหนักน้อย / ผอม \nภาวะเสี่ยงต่อโรค : มากกว่าคนปกติ"
        }else if(result >= 18.50 && result <= 22.99 ){
            selectedImage.setImageResource(R.drawable.select_2)
            binding.displayDetail.text = "อยู่ในเกณท์ : ปกติ (สุขภาพดี) \nภาวะเสี่ยงต่อโรค : เท่าคนปกติ"
        }
        else if(result >= 23 && result <= 24.99 ){
            selectedImage.setImageResource(R.drawable.select_3)
            binding.displayDetail.text = "อยู่ในเกณท์ : ท้วม / โรคอ้วนระดับ 1 \nภาวะเสี่ยงต่อโรค : อันตรายระดับ 1"
        }
        else if(result >= 25 && result <= 29.99 ){
            selectedImage.setImageResource(R.drawable.select_4)
            binding.displayDetail.text = "อยู่ในเกณท์ : อ้วน / โรคอ้วนระดับ 2 \nภาวะเสี่ยงต่อโรค : อันตรายระดับ 2"
        }
        else {
            selectedImage.setImageResource(R.drawable.select_5)
            binding.displayDetail.text = "อยู่ในเกณท์ : อ้วนมาก / โรคอ้วนระดับ 3 \nภาวะเสี่ยงต่อโรค : อันตรายระดับ 3"
        }
        display(result)

    }
    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if (keyCode == KeyEvent.KEYCODE_ENTER) {
            // Hide the keyboard
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }
        return false
    }

}